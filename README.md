# se358-LocalCafe

MERN stack web application for a coffee shop's online ordering system
Mongodb Atlas cloud database using aws


Running in terminal:
in project folder se358-LocalCafe run: "node server"
in client folder run: "npm i"
in client folder run: "npm start"
open localhost:3000 on browser


Web app was deployed using Heroku
view website here:
localcafe.herokuapp.com
