import './App.css';
import bootstrap from '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap'
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Navbar from './components/Navbar';
import AdminNavBar from './components/AdminNavbar';
import Cartscreen from './screens/Cartscreen';
import Loginscreen from './screens/Loginscreen';
import Registerscreen from './screens/Regsiterscreen';
import Ordersscreen from './screens/Ordersscreen';
import Adminscreen from './screens/Adminscreen';
import Welcomescreen from './screens/Welcomescreen';
import OrderPlacedScreen from './screens/OrderPlacedScreen';
import PageNotFound from './screens/PageNotFound';
function App() {
  return (
    <Router>
      <div className="App">
        <div className="content">
          <Switch>
            <Route exact path="/">
              <Navbar />
              <Welcomescreen />
            </Route>
            <Route path="/cart">
            <Navbar />
              <Cartscreen />
            </Route>
            <Route path="/login">
            <Navbar />
              <Loginscreen />
            </Route>
            <Route path="/register">
            <Navbar />
              <Registerscreen />
            </Route>
            <Route path="/orders">
            <Navbar />
              <Ordersscreen />
            </Route>
            <Route path="/orderplaced">
            <Navbar />
              <OrderPlacedScreen />
            </Route>
            <Route path="/admin">
              <AdminNavBar />
              <Adminscreen />
            </Route>
            <Route>
              <PageNotFound />
            </Route>
          </Switch>
          
        </div>
      </div>
    </Router>
  );
}
export default App;
