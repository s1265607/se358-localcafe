export const addToCart = (coffee, quantity, milk, flavor, temp) => (dispatch, getState) => {
    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * 
     charactersLength));
       }
       return result;
    }

    var cartItem = {
        name: coffee.name,
        _id: coffee._id,
        milk: milk,
        flavor: flavor,
        temp: temp,
        quantity: Number(quantity),
        prices: coffee.prices,
        price: coffee.prices[0][milk] * quantity,
        _cartid: makeid(5)
        
    }
   
   
    if (cartItem.quantity > 10) {
        alert('You cannot add more than 10 quantities')
    }
    else {
        if (cartItem.quantity < 1) {
            dispatch({ type: 'DELETE_FROM_CART', payload: coffee })
        }
        else {
            dispatch({ type: 'ADD_TO_CART', payload: cartItem })
        }
    }
    const cartItems = getState().cartReducer.cartItems
    localStorage.setItem('cartItems', JSON.stringify(cartItems))
}
export const deleteFromCart = (coffee) => (dispatch, getState) => {
    dispatch({ type: 'DELETE_FROM_CART', payload: coffee })
    const cartItems = getState().cartReducer.cartItems
    localStorage.setItem('cartItems', JSON.stringify(cartItems))
}

export const editCart = (coffee, quantity, milk, flavor, temp, _cartid) => (dispatch, getState) => {
    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * 
     charactersLength));
       }
       return result;
    }

    var cartItem = {
        name: coffee.name,
        _id: coffee._id,
        milk: milk,
        flavor: flavor,
        temp: temp,
        quantity: Number(quantity),
        prices: coffee.prices,
        price: coffee.prices[0][milk] * quantity,
        _cartid: _cartid
        
    }
   
   
    if (cartItem.quantity > 10) {
        alert('You cannot add more than 10 quantities')
    }
    else {
        if (cartItem.quantity < 1) {
            dispatch({ type: 'DELETE_FROM_CART', payload: coffee })
        }
        else {
            dispatch({ type: 'ADD_TO_CART', payload: cartItem })
        }
    }
    const cartItems = getState().cartReducer.cartItems
    localStorage.setItem('cartItems', JSON.stringify(cartItems))
}