const coffees =
    [
        {
            name: "Latte",
            milk: [
                "whole milk",
                "2% milk",
                "almond milk",
                "oat milk"
            ],
            flavor: [
                "none",
                "vanilla",
                "mocha",
                "caramel"
            ],
            temp: [
                "hot",
                "iced"
            ],
            prices: [
                {
                    "whole milk": 3.00,
                    "2% milk": 3.00,
                    "almond milk": 3.50,
                    "oat milk": 3.50
                }
            ]
        },
        {
            name: "Cold Brew",
            milk: [
                "none",
                "whole milk",
                "2% milk",
                "almond milk",
                "oat milk"
            ],
            flavor: [
                "none",
                "vanilla",
                "mocha",
                "caramel"
            ],
            temp: [
                "iced"
            ],
            prices: [
                {
                    "none": 2.00,
                    "whole milk": 2.00,
                    "2% milk": 2.00,
                    "almond milk": 2.50,
                    "oat milk": 2.50
                }
            ]
        },
        {
            name: "Cappuccino",
            milk: [
                "none",
                "whole milk",
                "2% milk",
                "almond milk",
                "oat milk"
            ],
            flavor: [
                "none",
                "vanilla",
                "mocha",
                "caramel"
            ],
            temp: [
                "hot",
                "iced"
            ],
            prices: [
                {
                    "none": 3.00,
                    "whole milk": 3.00,
                    "2% milk": 3.00,
                    "almond milk": 3.50,
                    "oat milk": 3.50
                }
            ]
        },
        {
            name: "Macchiato",
            milk: [
                "whole milk",
                "2% milk",
                "almond milk",
                "oat milk"
            ],
            flavor: [
                "none",
                "vanilla",
                "mocha",
                "caramel"
            ],
            temp: [
                "hot",
                "iced"
            ],
            prices: [
                {
                    "whole milk": 3.00,
                    "2% milk": 3.00,
                    "almond milk": 3.50,
                    "oat milk": 3.50
                }
            ]
        },
    ];
export default coffees