import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { logoutUser } from '../actions/userActions';
export default function Navbar() {
  const cartstate = useSelector(state => state.cartReducer)
  const userstate = useSelector(state => state.loginUserReducer)
  const { currentUser } = userstate
  const dispatch = useDispatch()
  return (
    <div >
      <nav className="navbar navbar-expand-lg ">
      
      
      
      
      <button class="navbar-toggler admin-button" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon textalign">Menu</span>
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul className="navbar-nav mx-auto">
          

            <li className="nav-item">
              <a className="nav-link nav-link-cart" href="/admin/coffeeslist">Coffees</a>
            </li>
            <li className="nav-item">
              <a className="nav-link nav-link-cart" href="/admin/addcoffee">Add Coffee</a>
            </li>
            <li className="nav-item">
              <a className="nav-link nav-link-cart" href="/admin/orderslist">Orders</a>
            </li>
            <li className="nav-item">
              <a className="nav-link nav-link-cart" href="/admin/userslist">Users</a>
            </li>

          </ul>
        </div>
      </nav>
    </div>
  )
}
