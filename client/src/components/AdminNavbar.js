import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { logoutUser } from '../actions/userActions';
export default function Navbar() {
  const cartstate = useSelector(state => state.cartReducer)
  const userstate = useSelector(state => state.loginUserReducer)
  const { currentUser } = userstate
  const dispatch = useDispatch()
  return (
    <div className='background'>
      <nav className="navbar navbar-expand-lg navbar-light  " role="navigation">
      <a className="navbar-brand" href="/admin/orderslist" >Local Cafe</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
          <i class="fas fa-bars"></i>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul className="navbar-nav ms-auto">
          
          <div className="dropdown nav-item">
                <a style={{ color: 'black' }} data-bs-toggle="dropdown" className="dropdown-toggle nav-link " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {currentUser.name}
                </a>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a className="dropdown-item" href="/admin/coffeeslist">Coffees</a>
                  <a className="dropdown-item" href="/admin/addcoffee">Add Coffee</a>
                  <a className="dropdown-item" href="/admin/orderslist">Orders</a>
                  <a className="dropdown-item" href="/admin/userslist">Users</a>
                  <a className="dropdown-item" href="#" onClick={() => dispatch(logoutUser())}><li>Logout</li></a>
                </div>


              </div>

          </ul>
        </div>
      </nav>
    </div>
  )
}