import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addToCart } from "../actions/cartActions";
export default function Coffee({ coffee }) {
    const [quantity, setquantity] = useState(1);
    const [milk, setmilk] = useState("wholemilk");
    const [flavor, setflavor] = useState("none");
    const [temp, settemp] = useState("hot");
    const dispatch = useDispatch()
    function addtocart() {
        dispatch(addToCart(coffee, quantity, milk, flavor, temp))
        alert("item added to cart")
    }
    var itemPrice = (coffee.prices[0][milk] * quantity);
    return (
        <div className='shadow-lg shadow-lg-home p-3 mb-5 bg-white rounded'>
            <h1>{coffee.name}</h1>

            <div className="flex-container">
                <div className="descr">
                    <p>Milk</p>
                    <select className='form-control form-control-home' value={milk} onChange={(e) => { setmilk(e.target.value) }}>
                        {coffee.milk.map((milk) => {
                            return <option value={milk}>{milk}</option>;
                        })}
                    </select>
                </div>
                <div className='descr'>
                    <p>Flavor</p>
                    <select className='form-control form-control-home' value={flavor} onChange={(e) => { setflavor(e.target.value) }}>
                        {coffee.flavor.map(flavor => {
                            return <option value={flavor}>{flavor}</option>;
                        })}
                    </select>
                </div>
                <div className='descr'>
                    <p>Hot/Iced</p>
                    <select className='form-control form-control-home' value={temp} onChange={(e) => { settemp(e.target.value) }}>
                        {coffee.temp.map(temp => {
                            return <option value={temp}>{temp}</option>;
                        })}
                    </select>
                </div>
                <div className='descr'>
                    <p>Quantity</p>
                    <select className='form-control form-control-home' value={quantity} onChange={(e) => { setquantity(e.target.value) }}>
                        {[...Array(10).keys()].map((x, i) => {
                            return <option value={i + 1}>{i + 1}</option>
                        })}
                    </select>
                </div>
            </div>
            <div className="flex-container">
                <div className="menu-price">
                    <h1 className="mt-1">
                        Price : ${parseFloat(itemPrice).toFixed(2)}
                    </h1>
                </div>

                <div className="menu-add">
                    <button className="btn btn-addtocart" onClick={addtocart}>ADD TO CART</button>
                </div>
            </div>
        </div >
    )
}
