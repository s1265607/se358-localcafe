import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux'
import { addCoffee } from "../actions/coffeeActions";
import Error from "../components/Error";
import Loading from "../components/Loading";
import Success from '../components/Success'
export default function Addcoffee() {
    const [name, setname] = useState("");
    const [wholemilkprice, setwholemilkprice] = useState();
    const [almondmilkprice, setalmondmilkprice] = useState();
    const [oatmilkprice, setoatmilkprice] = useState();
    const [twopercentmilkprice, settwopercentmilkprice] = useState();
    const flavor= [
        "none",
        "vanilla",
        "mocha",
        "caramel"
    ]
    const temp = [
        "hot",
        "iced"
    ]

    const dispatch = useDispatch()

    const addcoffeestate = useSelector(state => state.addCoffeeReducer)
    const { success, error, loading } = addcoffeestate
    function formHandler(e) {

        e.preventDefault();

        const coffee = {
            name,
            flavor,
            temp,
            prices: {
                wholemilk: wholemilkprice,
                twopercentmilk: twopercentmilkprice,
                almondmilk: almondmilkprice,
                oatmilk: oatmilkprice
            }
        }

        console.log(coffee);
        dispatch(addCoffee(coffee));

    }

    return (
        <div>
            <div className='text-left shadow-lg p-3 mb-5 bg-white rounded'>
                <h1>Add Coffee</h1>

                {loading && (<Loading />)}
                {error && (<Error error='Something went wrong' />)}
                {success && (<Success success='New Coffee added successfully' />)}

                <form onSubmit={formHandler}>
                    <input
                        className="form-control"
                        type="text"
                        placeholder="name"
                        value={name}
                        onChange={(e) => {
                            setname(e.target.value);
                        }}
                    />
                    <input
                        className="form-control"
                        type="text"
                        placeholder="whole milk price"
                        value={wholemilkprice}
                        onChange={(e) => {
                            setwholemilkprice(e.target.value);
                        }}
                    />
                    <input
                        className="form-control"
                        type="text"
                        placeholder="two percent milk price"
                        value={twopercentmilkprice}
                        onChange={(e) => {
                            settwopercentmilkprice(e.target.value);
                        }}
                    />
                    <input
                        className="form-control"
                        type="text"
                        placeholder="almond milk price"
                        value={almondmilkprice}
                        onChange={(e) => {
                            setalmondmilkprice(e.target.value);
                        }}
                    />
                    <input
                        className="form-control"
                        type="text"
                        placeholder="oat milk price"
                        value={oatmilkprice}
                        onChange={(e) => {
                            setoatmilkprice(e.target.value);
                        }}
                    />

                    
                    
                    <button className='btn mt-3' type='submit'>Add Coffee</button>
                </form>
            </div>
        </div>
    );
}
