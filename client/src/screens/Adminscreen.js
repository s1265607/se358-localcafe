import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Route, Switch } from "react-router-dom";
import { Link } from "react-router-dom";

import Addcoffee from "./AddCoffee";
import Editcoffee from "./EditCoffee";
import Edituser from "./EditUser";
import Orderslist from "./Orderslist";
import Coffeeslist from "./Coffeeslist";
import Userslist from "./Userslist";
import AdminMenu from "../components/AdminMenu";

export default function Adminscreen() {
  const userstate = useSelector((state) => state.loginUserReducer);
  const { currentUser } = userstate;
  const dispatch = useDispatch();

  useEffect(() => {
    if (!currentUser.isAdmin) {
      window.location.href = "/";
    }
  }, []);

  return (
    <div>
      <div className="row justify-content-center p-3">
        <div className="col-md-10">
          <h2 style={{ fontSize: "35px" }}>Admin Panel</h2>

         

        
          <Switch>
          <Route path="/admin" component={Userslist} exact/>
              <Route path="/admin/userslist" component={Userslist} exact/>
              <Route path="/admin/orderslist" component={Orderslist} exact/>
              <Route path="/admin/coffeeslist" component={Coffeeslist} exact/>
              <Route path="/admin/addcoffee" component={Addcoffee} exact/>
              <Route path="/admin/editcoffee/:coffeeid" component={Editcoffee} exact/>
              <Route path="/admin/edituser/:userid" component={Edituser} exact/>
          </Switch>
        </div>
      </div>
    </div>
  );
}
