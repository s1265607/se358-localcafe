import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { editCart, addToCart } from '../actions/cartActions'
import { deleteFromCart } from '../actions/cartActions'
import { placeOrder } from '../actions/orderActions'
export default function Cartscreen() {
    const cartstate = useSelector(state => state.cartReducer)
    const cartItems = cartstate.cartItems
    var subtotal = cartItems.reduce((x, item) => x + item.price, 0)
    const dispatch = useDispatch()

    function placeorder() {
        if (subtotal == 0) {
            alert("Cart is empty")
        }
        else {
            dispatch(placeOrder(subtotal))
        }
    }
    return (
        <div >
            <div className="row justify-content-center">
                <h2 style={{ fontSize: '40px' }}>Cart</h2>
                <hr />
                <div className="col-md-4 m-3">
                    {cartItems.map(item => {
                        return <div className="flex-container">
                            <div className='text-left m-1 w-100'>
                                <h1>{item.name}</h1>
                                <h3>milk: {item.milk}, flavor: {item.flavor}, temperature: {item.temp}</h3>
                                
                                <h3 style={{ display: 'inline' }}>Quantity </h3>
                                <i className="fa fa-minus" aria-hidden="true" onClick={() => { dispatch(editCart(item, item.quantity - 1, item.milk, item.flavor, item.temp, item._cartid)) }}></i>
                                <b>{item.quantity}</b>
                                <i className="fa fa-plus" aria-hidden="true" onClick={() => { dispatch(editCart(item, item.quantity + 1, item.milk, item.flavor, item.temp, item._cartid)) }}></i>
                                <h3>Price : ${parseFloat(item.price).toFixed(2)}</h3>
                               <h3></h3>
                                <i className="fa fa-trash" aria-hidden="true" onClick={() => { dispatch(deleteFromCart(item)) }} ></i>
                                <hr />
                            </div>
                           
                        </div>
                    })}
                    <div className="col-md-4">
                    </div>
                </div>
                <div className="col-md-4 text-right">
                <h3 style={{ fontSize: '35px' }}>Quantity: {cartstate.cartItems.length} </h3>
                    <h2 style={{ fontSize: '35px' }}>Total: ${parseFloat(subtotal).toFixed(2)} </h2>
                    <a className="nav-link" ><button className='btn btn-paynow' onClick={placeorder}>Pay Now</button></a>
                </div>
            </div>
        </div>
    )
}
