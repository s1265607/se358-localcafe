import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { deleteCoffee, getAllCoffees } from "../actions/coffeeActions";
import Error from "../components/Error";
import Loading from "../components/Loading";
export default function Coffeeslist() {
  const dispatch = useDispatch();

  const coffeesstate = useSelector((state) => state.getAllCoffeesReducer)

  const { coffees, error, loading } = coffeesstate;
  useEffect(() => {
    dispatch(getAllCoffees());
  }, [])
  return <div>
    <h2>Coffees List</h2>
    {loading && (<Loading/>)}
    {error && (<Error error='Something went wrong'/>)}

    <table  className='table table-bordered table-responsive-sm'>

        <thead className='thead-dark'>
            <tr>
                <th>Name</th>
                <th>Prices</th>
                <th>Temp</th>
                <th>Actions</th>
               
            </tr>
        </thead>
        <tbody>
        {coffees && coffees.map(coffee=>{

            return <tr>
                <td>{coffee.name}</td>
                <td>

                   Whole Milk : {coffee.prices[0]['wholemilk']} <br/>
                   Two Percent Milk : {coffee.prices[0]['twopercentmilk']} <br/>
                   Almond Milk : {coffee.prices[0]['almondmilk']} <br/>
                   Oat Milk : {coffee.prices[0]['oatmilk']} 
                    
                </td>
               
                <td>{coffee.temp[0]}<br />
                {coffee.temp[1]}</td>
                <td>
                <Link to={`/admin/editcoffee/${coffee._id}`} ><button className="btn btn-login">edit</button></Link>
                       <button onClick={()=>{dispatch(deleteCoffee(coffee._id))} } className="btn btn-login">delete</button>
                    </td>

            </tr>

        })}
        </tbody>

    </table>

   
  </div>;
}
