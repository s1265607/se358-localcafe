import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { editCoffee, getCoffeeById } from "../actions/coffeeActions";
import Error from "../components/Error";
import Loading from "../components/Loading";
import Success from "../components/Success";
export default function Editcoffee({ match }) {
  const dispatch = useDispatch();
  const [name , setname] = useState("");
  const [wholemilkprice, setwholemilkprice] = useState();
  const [twopercentmilkprice, settwopercentmilkprice] = useState();
  const [almondmilkprice, setalmondmilkprice] = useState();
  const [oatmilkprice, setoatmilkprice] = useState();
  const flavor= [
    "none",
    "vanilla",
    "mocha",
    "caramel"
]
const temp = [
    "hot",
    "iced"
]
  const getcoffeebyidstate = useSelector((state) => state.getCoffeeByIdReducer);
  const { coffee , error, loading } = getcoffeebyidstate;
  const editcoffeestate = useSelector((state) => state.editCoffeeReducer)
  const {editloading , editerror , editsuccess} = editcoffeestate;

  useEffect(() => {
    if(coffee)
    {
        if(coffee._id==match.params.coffeeid)
        {
            setname(coffee.name)
            setwholemilkprice(coffee.prices[0]['wholemilk'])
            settwopercentmilkprice(coffee.prices[0]['twopercentmilk'])
            setalmondmilkprice(coffee.prices[0]['almondmilk'])
            setoatmilkprice(coffee.prices[0]['oatmilk'])
        }
        else{
            dispatch(getCoffeeById(match.params.coffeeid));
        }
    }
    else{
        dispatch(getCoffeeById(match.params.coffeeid));
    }
  }, [coffee , dispatch]);
  function formHandler(e) {
    e.preventDefault();
    const editedcoffee = {
      _id : match.params.coffeeid,
      name,
      flavor,
      temp,
      prices: {
        wholemilk: wholemilkprice,
        twopercentmilk: twopercentmilkprice,
        almondmilk: almondmilkprice,
        oatmilk: oatmilkprice,
      },
    };
    dispatch(editCoffee(editedcoffee))
  }
  return (
    <div>
      <div className="text-left shadow-lg p-3 mb-5 bg-white rounded">
      <h1>Edit Coffee</h1>
        {loading && <Loading />}
        {error && <Error error="Something went wrong" />}
        {editsuccess && (<Success success='Coffee details edited successfully'/>)}
        {editloading && (<Loading />)}
        <form onSubmit={formHandler}>
          <input
            className="form-control"
            type="text"
            placeholder="name"
            value={name}
            onChange={(e) => {
              setname(e.target.value);
            }}
          />
          <input
            className="form-control"
            type="text"
            placeholder="whole milk price"
            value={wholemilkprice}
            onChange={(e) => {
              setwholemilkprice(e.target.value);
            }}
          />
          <input
            className="form-control"
            type="text"
            placeholder="two percent milk price"
            value={twopercentmilkprice}
            onChange={(e) => {
              settwopercentmilkprice(e.target.value);
            }}
          />
          <input
            className="form-control"
            type="text"
            placeholder="almond milk price"
            value={almondmilkprice}
            onChange={(e) => {
              setalmondmilkprice(e.target.value);
            }}
          />
           <input
            className="form-control"
            type="text"
            placeholder="oat milk price"
            value={oatmilkprice}
            onChange={(e) => {
              setoatmilkprice(e.target.value);
            }}
          />
          <button className="btn mt-3" type="submit">
            Edit Coffee
          </button>
        </form>
      </div>
    </div>
  );
}
