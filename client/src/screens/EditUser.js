import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { editUser, getUserById } from "../actions/userActions";
import Error from "../components/Error";
import Loading from "../components/Loading";
import Success from "../components/Success";
export default function Edituser({ match }) {
  const dispatch = useDispatch();
  const [name, setname] = useState("");
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");
  const [isAdmin, setisadmin] = useState("");
  
  const getuserbyidstate = useSelector((state) => state.getUserByIdReducer);

  const { user , error, loading } = getuserbyidstate;

  const edituserstate = useSelector((state) => state.editUserReducer)
  const {editloading , editerror , editsuccess} = edituserstate;

  useEffect(() => {
    if(user)
    {
        if(user._id==match.params.userid)
        {
            setname(user.name)
            setemail(user.email)
            setpassword(user.password)
            setisadmin(user.isAdmin) 
        }
        else{
            dispatch(getUserById(match.params.userid));
        }
    }
    else{
        dispatch(getUserById(match.params.userid));
    }
  }, [user , dispatch]);

  function formHandler(e) {
    e.preventDefault();

    const editeduser = {
      _id : match.params.userid,
      name,
      email,
      password,
      isAdmin,
    };

    dispatch(editUser(editeduser))
  }
  return (
    <div>
      <div className="text-left shadow-lg p-3 mb-5 bg-white rounded">
      <h1>Edit User</h1>
        {loading && <Loading />}
        {error && <Error error="Something went wrong" />}
        {editsuccess && (<Success success='Coffee details edited successfully'/>)}
        {editloading && (<Loading />)}
        <form onSubmit={formHandler}>
          <input
            className="form-control"
            type="text"
            placeholder="name"
            value={name}
            onChange={(e) => {
              setname(e.target.value);
            }}
          />
          <input
            className="form-control"
            type="text"
            placeholder="email"
            value={email}
            onChange={(e) => {
              setemail(e.target.value);
            }}
          />
          <input
            className="form-control"
            type="text"
            placeholder="password"
            value={password}
            onChange={(e) => {
              setpassword(e.target.value);
            }}
          />
          <input
            className="form-control"
            type="text"
            placeholder="employee access"
            value={isAdmin}
            onChange={(e) => {
              setisadmin(e.target.value);
            }}
          />
           
          
          <button className="btn mt-3" type="submit">
            Edit User
          </button>
        </form>
      </div>
    </div>
  );
}
