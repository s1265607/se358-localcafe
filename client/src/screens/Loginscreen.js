import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'
import { faEye } from "@fortawesome/free-solid-svg-icons";
import Loading from "../components/Loading";
import Error from "../components/Error";
import { loginUser } from "../actions/userActions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
const eye = <FontAwesomeIcon icon={faEye} />
//import Error from "../components/Error";
//import Loading from "../components/Loading";
export default function Loginscreen() {
  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');
  const [passwordShown, setPasswordShown] = useState(false);
  const togglePasswordVisiblity = () => {
    setPasswordShown(passwordShown ? false : true);
  };
  const loginstate = useSelector(state => state.loginUserReducer)
  const {loading , error} = loginstate
  const dispatch = useDispatch()
  useEffect(() => {

    if (localStorage.getItem('currentUser')) {
      window.location.href = '/menu'
    }

  }, [])

  function login() {
    const user = { email, password }
    dispatch(loginUser(user))
  }

  return (
    <div className='login'>
      <div className="row justify-content-center mt-4">
        <div className="col-md-4 mt-5 text-left shadow-lg shadow-lg-login p-3 mb-5 bg-white rounded">
          <h2 className="text-center m-2" style={{ fontSize: "35px" }}>
            Login
          </h2>
          
          <div>
            <input required type="text" placeholder="email" className="form-control" value={email} onChange={(e) => { setemail(e.target.value) }} />
            <div className="pass-wrapper">
              <input
                type={passwordShown ? "text" : "password"}
                name="password"
                placeholder="password"
                className="form-control"
                value={password}
                required
                onChange={(e) => { setpassword(e.target.value) }}
              />
              <i className="icon-register" onClick={togglePasswordVisiblity}>{eye}</i>
            </div>
            {loading && (<Loading />)}
          {error && (<Error error = 'invalid credentials' />)}
            <button onClick={login} className="btn btn-login">LOGIN</button>
            <br />
            <a style={{ color: 'black' }} href="/register" className="btn mt-2">Click Here To Register</a>
          </div>
          
        </div>
      </div>
    </div>
  )
}
