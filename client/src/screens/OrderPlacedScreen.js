import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import Nav from 'react-bootstrap/Nav'

export default function Homescreen() {
    const userstate = useSelector(state => state.loginUserReducer)
    const { currentUser } = userstate
    return (

        <div className="row">
            <h1>Order Placed Successfully!</h1>
            <div>
                <a href='/'>Return to home</a>
            </div>
           
        </div>

    )
}
    
