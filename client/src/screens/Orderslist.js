import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deliverOrder, getAllOrders, deleteOrder } from "../actions/orderActions";
import Error from "../components/Error";
import Loading from "../components/Loading";
export default function Orderslist() {
  const dispatch = useDispatch();
  const getordersstate = useSelector((state) => state.getAllOrdersReducer);
  const { loading, error, orders } = getordersstate;
  useEffect(() => {
    dispatch(getAllOrders());
  }, []);
  return (
    <div className="order-list">

      
       <h2>Pending Orders List</h2>
      {loading && <Loading />}
      {error && <Error error="Something went wrong" />}
      <table className="table table-striped table-bordered table-responsive-sm">
        <thead className="thead-dark">
          <tr>
            <th>Order Id</th>
            <th>Email</th>
            <th>User Id</th>
            <th>Order Items</th>
            <th>Amount</th>
            <th>Date</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>

        <tbody>
          {orders &&
            orders.map((order) => {
              if(!(order.isDelivered)){
              return (
                <tr>
                  <td>{order._id}</td>
                  <td>{order.email}</td>
                  <td>{order.userid}</td>
                  <td className="order-items">
                  {order.orderItems.map(item=>{
                                        return <div>
                                            <p>{item.name}, temp: {item.temp}, milk: {item.milk}, quantity: {item.quantity}</p>
                                        </div>
                                    })}
                  </td>
                  <td>${parseFloat(order.orderAmount).toFixed(2)}
                  </td>
                  <td>{order.createdAt.substring(0, 10)}</td>
                  <td>
                    {order.isDelivered ? (
                      <h3 className="btn">Done</h3>
                    ) : (
                      <button className="btn" onClick={()=>{dispatch(deliverOrder(order._id))}}>Pending</button>
                    )}
                  </td>
                  <td>
                  
                       <button onClick={()=>{dispatch(deleteOrder(order._id))} } className="btn btn-login">delete</button>
                   
                    </td>
                </tr>
              );
                    }
            })}
        </tbody>
      </table>

      <h2>Delivered Orders List</h2>
      {loading && <Loading />}
      {error && <Error error="Something went wrong" />}
      <table className="table table-striped table-bordered table-responsive-sm">
        <thead className="thead-dark">
          <tr>
            <th>Order Id</th>
            <th>Email</th>
            <th>User Id</th>
            <th>Order Items</th>
            <th>Amount</th>
            <th>Date</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>

        <tbody>
          {orders &&
            orders.map((order) => {
              if(order.isDelivered){
              return (
                <tr>
                  <td>{order._id}</td>
                  <td>{order.email}</td>
                  <td>{order.userid}</td>
                  <td className="order-items">
                  {order.orderItems.map(item=>{
                                        return <div>
                                            <p>{item.name}, milk: {item.milk}, quantity: {item.quantity}</p>
                                        </div>
                                    })}
                  </td>
                  <td>${parseFloat(order.orderAmount).toFixed(2)}
                  </td>
                  <td>{order.createdAt.substring(0, 10)}</td>
                  <td>
                    {order.isDelivered ? (
                      <h3 className="btn">Done</h3>
                    ) : (
                      <button className="btn" onClick={()=>{dispatch(deliverOrder(order._id))}}>Pending</button>
                    )}
                  </td>
                  <td><button onClick={()=>{dispatch(deleteOrder(order._id))} } className="btn btn-login">delete</button></td>
                </tr>
              );
                    }
            })}
        </tbody>
      </table>

    </div>
  );
}
