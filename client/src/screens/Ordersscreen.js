import React , {useState, useEffect} from 'react'
import {useDispatch , useSelector} from 'react-redux'
import { getUserOrders } from '../actions/orderActions'
import Error from "../components/Error";
import Loading from "../components/Loading";
import Success from '../components/Success'
import AOS from 'aos'
import 'aos/dist/aos.css';
export default function Ordersscreen() {
    
    AOS.init()
    const dispatch = useDispatch()
    const orderstate = useSelector(state=>state.getUserOrdersReducer)
    const {orders , error , loading} = orderstate

    useEffect(() => {

        dispatch(getUserOrders())
      
    }, [])

    return (
        <div>
            <h2 style={{fontSize:'35px'}}>My Orders</h2>
            <hr/>
            <div className="row justify-content-center">
                {loading && (<Loading/>)}
                {error && (<Error error='Something went wrong'/>)}
                {orders && orders.map(order=>{
                    return <div className="col-md-8 m-2 p-1" data-aos='fade-down'  >

                            <div className="flex-container">
                                <div className='text-left w-100 m-1'>
                                    <h2 style={{fontSize:'25px'}}>Items</h2>
                                    <hr/>
                                    {order.orderItems.map(item=>{
                                        return <div>
                                            <p>{item.name} <br></br>Temp: {item.temp} <br></br>Milk: {item.milk} <br></br>Quantity: {item.quantity} <br></br>Price = ${item.price}</p>
                                        </div>
                                    })}
                                </div>
                                
                                <div className='text-left w-100 m-1'>
                                <h2 style={{fontSize:'25px'}}>Order Info</h2>
                                <hr/>
                                <p>Order Amount : ${parseFloat(order.orderAmount).toFixed(2)}</p>
                                <p>Date : {order.createdAt.substring(0,10)}</p>
                                <p>Order Id : {order._id}</p>
                                <p>Status : {order.isDelivered ?(
                                    <p>Done</p>
                                ):(
                                    <p>Pending</p>
                                )}</p>
                                </div>
                            </div>

                    </div>
                })}
            </div>
        </div>
    )
}
