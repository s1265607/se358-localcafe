import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { registerUser } from "../actions/userActions";
import { useForm } from "react-hook-form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import Loading from "../components/Loading";
import Success from "../components/Success";
import Error from "../components/Error";
const eye = <FontAwesomeIcon icon={faEye} />;

export default function Registerscreen() {
    const [name, setname] = useState('')
    const [email, setemail] = useState('')
    const [password, setpassword] = useState('')
    const [cpassword, setcpassword] = useState('')
    const [passwordShown, setPasswordShown] = useState(false);
    const togglePasswordVisiblity = () => {
        setPasswordShown(passwordShown ? false : true);
    };
    const [cpasswordShown, setCPasswordShown] = useState(false);
    const toggleCPasswordVisiblity = () => {
        setCPasswordShown(cpasswordShown ? false : true);
    };
    const registerstate = useSelector(state=>state.registerUserReducer)
    const {error, loading, success} = registerstate
    const dispatch = useDispatch()
    function register() {
        if((name =='')|| (email == '') || (password == '')|| (cpassword == '')){
            alert("all fields are required")
        }
        else if (password != cpassword) {
            alert("passwords not matched")
        }
        else {
            const user = {
                name,
                email,
                password
            }
            console.log(user)
            dispatch(registerUser(user))
        }
    }
    return (
        <div className='register'>
            <div className="row justify-content-center mt-4">
                <div className="col-md-4 mt-5 text-left shadow-lg shadow-lg-login p-3 mb-5 bg-white rounded">

                {loading && (<Loading />)}
                    {success && (<Success success = 'User registered successfully'/>)}
                    {error && (<Error error = 'Email already registered' />)}
                    
                    <h2 className="text-center m-2" style={{ fontSize: "35px" }}>
                        Register
                    </h2>

                    <div>
                        <input required type="text" placeholder="name" className="form-control" value={name}
                            onChange={(e) => { setname(e.target.value) }} />
                        <div className="row justify-content-center mt-4"></div>
                        <input required type="text" placeholder="email" className="form-control" value={email}
                            onChange={(e) => { setemail(e.target.value) }} />
                        <div className="row justify-content-center mt-4"></div>
                        <div className="pass-wrapper">
                            <input
                                type={passwordShown ? "text" : "password"}
                                name="password"
                                placeholder="password"
                                className="form-control"
                                value={password}
                                required
                                onChange={(e) => { setpassword(e.target.value) }}
                            />
                            <i className="icon-register" onClick={togglePasswordVisiblity}>{eye}</i>
                        </div>
                        <div className="pass-wrapper">
                            <input
                                type={cpasswordShown ? "text" : "password"}
                                name="cpassword"
                                placeholder="confirm password"
                                className="form-control"
                                value={cpassword}
                                required
                                onChange={(e) => { setcpassword(e.target.value) }}
                            />
                            <i className="icon-register" onClick={toggleCPasswordVisiblity}>{eye}</i>

                           
                        </div>
                        <div className="row justify-content-center mt-4"></div>
                        <button onClick={register} className="btn btn-login mt-3 mb-3">REGISTER</button>
                        <br />
                        <a style={{ color: 'black' }} className="btn mt-2" href="/login">Click Here To Login</a>
                    </div>
                </div>
            </div>
        </div>
    );
}
