import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getAllOrders } from "../actions/orderActions";
import Error from "../components/Error";
import Loading from "../components/Loading";
import {deleteUser, getAllUsers} from '../actions/userActions'
export default function Userslist() {
    const dispatch = useDispatch()
  const usersstate = useSelector(state=>state.getAllUsersReducer)
  const {error , loading , users} = usersstate
    useEffect(() => {

        dispatch(getAllUsers())
        
    }, [])
    return (
        <div>

            <h2>Users List</h2>
            {loading && <Loading />}
      {error && <Error error="Something went wrong" />}
       <table className='table table-striped table-bordered table-responsive-sm user-list'>
           <thead className='thead-dark'>
         <tr>
             <th>User Id</th>
             <th>Name</th>
             <th>Email</th>
             <th>Employee Access</th>
             <th>Actions</th>
         </tr>
           </thead>

           <tbody>
               {users && users.map(user=>{
                   return <tr>
                       <td>{user._id}</td>
                       <td>{user.name}</td>
                       <td>{user.email}</td>
                       <td>{user.isAdmin.toString()}
                       </td>
                       <td>
                       <Link to={`/admin/edituser/${user._id}`} ><button className="btn btn-login">edit</button></Link>
                       <button onClick={()=>{dispatch(deleteUser(user._id))} } className="btn btn-login">delete</button>
                       </td>
                   </tr>
               })}
           </tbody>

       </table>

        </div>
    )
}
