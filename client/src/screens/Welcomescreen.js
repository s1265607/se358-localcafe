import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import Nav from 'react-bootstrap/Nav'
import Homescreen from './Homescreen';
import { getAllCoffees } from '../actions/coffeeActions'
import Loading from '../components/Loading'
import Error from '../components/Error'
import Coffee from '../components/Coffee'
export default function Welcomescreen() {
    const userstate = useSelector(state => state.loginUserReducer)
    const { currentUser } = userstate
    const dispatch = useDispatch()
const coffeesstate = useSelector(state => state.getAllCoffeesReducer)
const { coffees, error, loading } = coffeesstate;
useEffect(() => {
    dispatch(getAllCoffees())
}, [])
    return (
        <div>
            {currentUser ? (



    <div>
        <div className="row">
            {loading ? (<Loading />) : error ? (<Error error='Something went wrong' />) : (
                coffees.map(coffee => {
                    return (
                        <div className="col" key={coffee._id}>
                            <div >
                                <Coffee coffee={coffee} />
                            </div>
                        </div>
                    )
                })
            )}
        </div>
    </div>






            ) : (
                <div>
                    <h2 style={{padding: '1%'}}>Welcome!</h2>
                    <h3 style={{padding: '1%'}}>Create an account to place orders online and pick up in store!</h3>
                    <div>
                    <a style={{ color: 'black' , padding: '1%' }} href="/register" className="btn mt-2 welcome">Click Here To Register</a>
                    </div>
                    
                    <h3 style={{padding: '1%'}}>Already have an account?</h3>
                    <div>
                    <a style={{ color: 'black' , padding: '1%'}} href="/login" className="btn mt-2 welcome">Click Here To Login</a>
                    
                    </div>
                </div>

            )}
        </div>

    )
}
