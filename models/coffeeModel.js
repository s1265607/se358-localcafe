const mongoose = require("mongoose");
const coffeeSchema = mongoose.Schema({
    name: { type: String, require },
    milk: [],
    flavor: [],
    temp: [],
    prices: []

}, {
    timestamps: true,
})

const coffeeModel = mongoose.model('coffees', coffeeSchema)

module.exports = coffeeModel