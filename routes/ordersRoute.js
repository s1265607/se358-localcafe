const express = require("express");
const router = express.Router();
const Order = require('../models/orderModel')

router.post("/placeorder", async(req, res) => {
  
  const {subtotal , currentUser , cartItems} = req.body
  const newOrder= new Order({
      name : currentUser.name,
    email : currentUser.email ,
    userid : currentUser._id ,
    orderItems : cartItems , 
    orderAmount : subtotal,
    
})



  try {
     newOrder.save()
     res.send('Order placed successfully')
  } catch (error) {
      return res.status(400).json({ message: 'Something went wrong' + error});
  }

});


router.post("/getuserorders", async(req, res) => {
  const {userid} = req.body
  try {
      const orders = await Order.find({userid : userid}).sort({createdAt: -1})
      res.send(orders)
  } catch (error) {
      return res.status(400).json({ message: 'Something went wrong' });
  }
});

router.get("/getallorders", async(req, res) => {

     try {
         const orders = await Order.find({}).sort({isDelivered: 1}).sort({createdAt: 1})
         res.send(orders)
     } catch (error) {
         return res.status(400).json({ message: error});
     }

});

router.post("/deliverorder", async(req, res) => {

    const orderid = req.body.orderid
    try {
        const order = await Order.findOne({_id : orderid})
        order.isDelivered = true
        await order.save()
        res.send('Order Delivered Successfully')
    } catch (error) {

        return res.status(400).json({ message: error});
        
    }
  
});

router.post("/deleteorder", async(req, res) => {

    const orderid = req.body.orderid
    try {
        const order = await Order.findOneAndDelete({_id : orderid})
        res.send('Order Deleted Successfully')
    } catch (error) {

        return res.status(400).json({ message: error});
        
    }
  
});



module.exports = router
