const express = require("express");
const Coffee = require('./models/coffeeModel')
const db = require("./db.js")
const app = express();
const path = require("path");


app.use(express.json());
const coffeesRoute = require('./routes/coffeesRoute')
const userRoute = require('./routes/userRoute')
const ordersRoute = require('./routes/ordersRoute')
app.use('/api/coffees/', coffeesRoute)
app.use('/api/users/', userRoute)
app.use('/api/orders/', ordersRoute)
/*
app.get("/", (req, res) => {
    res.send("Server working on " + port);
});
*/

app.use(express.static(path.resolve(__dirname, "./client/build")));
app.get("*", function (request, response) {
    response.sendFile(path.resolve(__dirname, "./client/build", "index.html"));
  });

let port = process.env.PORT;
if(port == null || port == ""){
    port = 8000;
}
app.listen(port, ()=>{
    console.log('App listening on '+ port)
})